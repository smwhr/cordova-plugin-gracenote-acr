#import <Cordova/CDVPlugin.h>

// subset of ACR/EPG/Video classes
#import <AVFoundation/AVFoundation.h>
#import <GracenoteACR/GnACR.h>
#import <GracenoteACR/GnVideo.h>
#import <GracenoteACR/GnVideoTitle.h>
#import <GracenoteACR/GnVideoWork.h>
#import <GracenoteACR/GnSdkManager.h>
#import <GracenoteACR/GnUser.h>
#import <GracenoteACR/GnLocale.h>
#import <GracenoteACR/GnResult.h>
#import <GracenoteACR/GnLocaleSetting.h>
#import <GracenoteACR/GnAcrMatch.h>
#import <GracenoteACR/IGnAcrResultDelegate.h>
#import <GracenoteACR/IGnAcrStatusDelegate.h>
#import <GracenoteACR/GnAcrStatus.h>
#import <GracenoteACR/GnAcrAudioConfig.h>
#import <GracenoteACR/GnAcrAudioConfig.h>
#import <GracenoteACR/GnEPG.h>
#import <GracenoteACR/GnStorageSqlite.h>
#import <GracenoteACR/GnAudioSourceiOSMic.h>


// subset of Music ID classes
#import <GracenoteACR/GnMusicID.h>
#import <GracenoteACR/GnTrack.h>

#import <GracenoteACR/GnFPCache.h>

#import "CustomerCredentials.h"

#define SAVED_USER_KEY_ACR @"savedUserACR"
#define SAVED_USER_KEY_MUSIC @"savedUserMUSIC"
#define ADAPTIVE_TAG  12

@interface TRYEntourageACR : CDVPlugin <IGnAcrResultDelegate, IGnAcrStatusDelegate, AVAudioSessionDelegate, GnAudioSourceDelegate, GnFPCacheSourceDelegate> {

    GnACR               *mACR;
    GnSdkManager        *mManager;
    GnUser              *mUserACR;
    GnAudioSourceiOSMic *mSource;
    
    // for followup query
    GnAcrMatch          *mLatestAcrMatch;
    
    // for loading FP bundles
    NSFileHandle        *bundleFileHandle;
    BOOL                mIsAdaptive;
    
    NSString            *currentACRResultCallback;
}


- (void)TRYInit:(CDVInvokedUrlCommand*)command;
- (void)TRYStart:(CDVInvokedUrlCommand*)command;
- (void)TRYStop:(CDVInvokedUrlCommand*)command;
- (void)TRYLookup:(CDVInvokedUrlCommand*)command;


@end