#import "TRYEntourageACR.h"
#import <Cordova/CDV.h>

@implementation TRYEntourageACR

/* start app with GN credentials */
-(void) TRYInit:(CDVInvokedUrlCommand *)command{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSError *error = nil;
        [self initManager:&error];
        
        if (error){
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Initialization failure"];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            return;
        }
        NSLog(@"%s","Manager inited");
        
        GnUser *acrUser = [self getUserACR];
        if(!acrUser){
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"User failure"];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            return;
        }
        NSLog(@"%s","User inited");
        
        mACR = [[GnACR alloc] initWithUser:acrUser
                                     error:&error];
        
        mACR.statusDelegate = self;
        mACR.resultDelegate = self;
        
        //SET YES IN PRODUCTION
        mACR.lookupCacheOnly = NO;
        
        if (error) {
            NSString *msg = [NSString stringWithFormat:@"ACR init ERROR: %@ (0x%x)", error.localizedDescription, error.code];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:msg];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            return;
        }
        
        GnAcrAudioConfig *config =
        [[GnAcrAudioConfig alloc] initWithAudioSourceType:GnAcrAudioSourceMic
                                               sampleRate:GnAcrAudioSampleRate44100 // only 8000, and 44100 are supported
                                                   format:GnAcrAudioSampleFormatPCM16
                                              numChannels:1];
        
        error = [mACR audioInitWithAudioConfig:config];
        if (error) {
            NSString *msg = [NSString stringWithFormat:@"Audio init ERROR: %@ (0x%x)", error.localizedDescription, error.code];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:msg];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            return;
        }
        NSLog(@"%s","Audio inited");
        
        [mACR setOptimizationMode:GnAcrOptimizationDefault];
        
        mSource = [[GnAudioSourceiOSMic alloc] initWithAudioConfig:config];
        mSource.audioDelegate = self;
        
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"1"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
  
  }];

}

/* start live mode (start sound and stuffs) */
- (void)TRYStart:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        NSError *error = nil;
        CDVPluginResult* pluginResult = nil;
        
        // Start the audio Source
        error = [mSource start];
        if (error) {
            NSString *msg = [NSString stringWithFormat:@"Source Start ERROR: %@ (0x%x)", error.localizedDescription, error.code];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:msg];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            return;
        }
        NSLog(@"%s","Source started inited");
        
        [[AVAudioSession sharedInstance] setDelegate:self];

        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"1"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)TRYStop:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        [[AVAudioSession sharedInstance] setDelegate:nil];
        
        NSError *error = nil;
        CDVPluginResult* pluginResult = nil;
        
        error = [mSource stop];
        NSLog(@"%s","Source stopped");
        //mSource = nil;
        //mACR = nil;
        
        NSString *msg;
        if (error){
            msg = [NSString stringWithFormat:@"Source Stop ERROR: %@ (0x%x)", error.localizedDescription, error.code];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:msg];
        }else{
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"1"];
        }
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

/* force perform a lookup */
- (void)TRYLookup:(CDVInvokedUrlCommand*)command {
    
    id lookupSource = [command.arguments objectAtIndex:0];
    currentACRResultCallback = command.callbackId;
    
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        if ([lookupSource isEqualToString:@"local"]) {
            //mACR.lookupCacheOnly = YES;
        }
        
        NSError *error = [mACR doManualLookup];
        
        if(error){
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
    }];
}


/* utilities function : most are take straight off the sample Entourage app */

- (void)initManager:(NSError **)error
{
    if (!mManager) {
        
        mManager = [[GnSdkManager alloc] initWithLicense:LICENSE_STRING error:error];
        if(mManager){
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory,
                                                                 NSUserDomainMask,
                                                                 YES);
            NSString *cacheFolderPath = [paths objectAtIndex:0];
            [GnStorageSqlite setStorageFolder:cacheFolderPath error:error];
            // [GnStorageSqlite setStorageFileSize:10240 error:error];     // 5 MB
            [GnStorageSqlite setStorageMemorySize:512 error:error];    // 512 KB
            [GnFPCache setStorageFolder:cacheFolderPath error:error];
            
            // now we can ingest any bundles we have available
            // [self loadFPBundle];
        }
    }
}

-(GnUser*)getUserACR
{
    // It is best practice to cache users after the first launch of the app.
    // This sample code shows how to serialize and save the user for subsequent launches.
    
    if (!mUserACR) {
        NSError *error = nil;
        NSString *savedUser = [[NSUserDefaults standardUserDefaults] objectForKey:SAVED_USER_KEY_ACR];
        
        if (savedUser)
        {
            mUserACR = [[GnUser alloc] initWithSerializedUser:savedUser error:&error];
        }
        else
        {
            mUserACR = [[GnUser alloc] initWithClientId:CLIENT_ID_VIDEO
                                            clientIdTag:CLIENT_TAG_VIDEO
                                             appVersion:@"0.0.1"
                                       registrationType:GnUserRegistrationType_NewUser error:&error];
            
            if (mUserACR) {
                // this should be saved somewhere secure like the keychain,
                // but this is outside the scope of the sample code.
                [[NSUserDefaults standardUserDefaults] setObject:mUserACR.serializedUser
                                                          forKey:SAVED_USER_KEY_ACR];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
        
        if (error) {
            NSLog(@"%@",error);
            return nil;
        }
        
    }
    return mUserACR;
}


//implementation of delegates
-(void)audioBytesReady:(void const * const)bytes length:(int)length
{
    NSError *error = nil;
    error = [mACR writeBytes:bytes length:length];
    if (error) {
        NSLog(@"audioBytesReady error: %@", error);
    }
    
}



-(void)acrResultReady:(GnResult*)result
{
    @autoreleasepool {
        // ACR query results will be returned in this callback
        // Below is an example of how to access the result metadata.
        
        // These callbacks may occur on threads other than the main thread.
        // Be careful not to block these callbacks for long periods of time.
        
        
        // Get the enumerator to access the ACR Match objects (GnAcrMatch)
        NSEnumerator *matches = result.acrMatches;
        int count = 0;
        
        // for each GnAcrMatch returned in the GnResult
        for (GnAcrMatch *match in matches) {
            count++;
            
            // Get the title and subtitle from this GnAcrMatch
            NSString *acrTitle = match.title.display;
            NSString *acrSubtitle = match.subtitle.display;
            NSString *channelCallsign = @"DVD";
            
            if (!acrSubtitle) acrSubtitle = @"";
            
            
            // Retreive the GnTvAiring from the GnAcrMatch
            GnTvAiring *airing = match.tvAiring;
            
            if(airing) {
                // Retreive the GnTvChannel from the GnTvAiring
                GnTvChannel *channel = airing.channel;
                channelCallsign = channel.callsign;
            }
            
            
            // Get the position (ms from beginning of work/program) of the GnAcrMatch
            NSString* contentPosition = match.adjustedPosition;
            
            if(match.customData){
                NSLog(@"%@",match.customData);
            }else{
                NSLog(@"%s","no customData");
            }
            
            GnVideoWork *work = match.avWork;
            if(work){
                acrTitle = work.title.display;
                contentPosition = match.adjustedPosition;
            }else{
                NSLog(@"%s","no GnVideoWork found");
            }
            
            
            NSDictionary *matchedItem = [ [NSDictionary alloc]
                                         initWithObjectsAndKeys :
                                         channelCallsign, @"channel",
                                         acrTitle, @"title",
                                         acrSubtitle, @"subtitle",
                                         contentPosition, @"tc",
                                         nil
                                         ];
            NSLog(@"%@",matchedItem);
            CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:matchedItem];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:currentACRResultCallback];
        }
        
        if (count == 0) {
            NSLog(@"%@",@"Nothing found");
            CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Nothing found"];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:currentACRResultCallback];
        }
        
    }
    
}

#define TRANSITION_KEY  @"TRANSITION"

#define SILENCE_KEY     @"SILENCE"
#define NSM_KEY         @"NOISE,SPEECH,MUSIC"
#define RATIO_KEY       @"RATIO"

#define LOCAL_KEY       @"LOCAL"
#define ONLINE_KEY      @"ONLINE"

#define NETWORK_KEY     @"NETWORK"
#define DEBUG_KEY       @"DEBUG"

#define ERROR_KEY       @"ERROR"
#define FP_KEY          @"FP"
#define MODE_KEY        @"MODE"

-(void)acrStatusReady:(GnAcrStatus*)status
{
    @autoreleasepool {
        // This status callback will be called periodically with status from the ACR subsystem
        // You can use these statuses as you like.
        
        // These callbacks may occur on threads other than the main thread.
        // Be careful not to block these callbacks for long periods of time.
        
        
        NSString *message = nil;
        BOOL doShowStatus = YES;
        
        switch (status.statusType) {
            case GnAcrStatusTypeSilent:
                doShowStatus = ![[NSUserDefaults standardUserDefaults] boolForKey:SILENCE_KEY];
                message = [NSString stringWithFormat:@"Silence %10.2f", status.value];
                break;
            case GnAcrStatusTypeSilentRatio:
                doShowStatus = ![[NSUserDefaults standardUserDefaults] boolForKey:RATIO_KEY];
                message = [NSString stringWithFormat:@"Silence ratio %10.3f", status.value];
                break;
            case GnAcrStatusTypeError:
                doShowStatus = ![[NSUserDefaults standardUserDefaults] boolForKey:ERROR_KEY];
                message = [NSString stringWithFormat:@"ERROR %@ (0x%x)", [status.error localizedDescription], status.error.code];
                break;
            case GnAcrStatusTypeNoMatchMode:
                doShowStatus = ![[NSUserDefaults standardUserDefaults] boolForKey:MODE_KEY];
                message = [NSString stringWithFormat:@"No Match Mode %10.0f secs between queries", status.value];
                break;
            case GnAcrStatusTypeConnecting:
                doShowStatus = ![[NSUserDefaults standardUserDefaults] boolForKey:NETWORK_KEY];
                message = [NSString stringWithFormat:@"Connecting"];
                break;
            case GnAcrStatusTypeFingerprintGenerated:
                doShowStatus = ![[NSUserDefaults standardUserDefaults] boolForKey:FP_KEY];
                message = [NSString stringWithFormat:@"Fingerprint Generated"];
                break;
            case GnAcrStatusTypeFingerprintStarted:
                doShowStatus = ![[NSUserDefaults standardUserDefaults] boolForKey:FP_KEY];
                message = [NSString stringWithFormat:@"Fingerprint Started"];
                break;
            case GnAcrStatusTypeLocalLookupComplete:
                doShowStatus = ![[NSUserDefaults standardUserDefaults] boolForKey:LOCAL_KEY];
                message = [NSString stringWithFormat:@"Local Lookup Complete"];
                break;
            case GnAcrStatusTypeMusic:
                doShowStatus = ![[NSUserDefaults standardUserDefaults] boolForKey:NSM_KEY];
                message = [NSString stringWithFormat:@"Music"];
                break;
            case GnAcrStatusTypeNetworkReceiving:
                doShowStatus = ![[NSUserDefaults standardUserDefaults] boolForKey:NETWORK_KEY];
                message = [NSString stringWithFormat:@"Network Receiving"];
                break;
            case GnAcrStatusTypeNetworkSending:
                doShowStatus = ![[NSUserDefaults standardUserDefaults] boolForKey:NETWORK_KEY];
                message = [NSString stringWithFormat:@"Network Sending"];
                break;
            case GnAcrStatusTypeNoise:
                doShowStatus = ![[NSUserDefaults standardUserDefaults] boolForKey:NSM_KEY];
                message = [NSString stringWithFormat:@"Noise"];
                break;
            case GnAcrStatusTypeSpeech:
                doShowStatus = ![[NSUserDefaults standardUserDefaults] boolForKey:NSM_KEY];
                message = [NSString stringWithFormat:@"Speech"];
                break;
            case GnAcrStatusTypeNormalMatchMode:
                doShowStatus = ![[NSUserDefaults standardUserDefaults] boolForKey:MODE_KEY];
                message = [NSString stringWithFormat:@"Normal Match Mode"];
                break;
            case GnAcrStatusTypeOnlineLookupComplete:
                doShowStatus = ![[NSUserDefaults standardUserDefaults] boolForKey:ONLINE_KEY];
                message = [NSString stringWithFormat:@"Online Lookup Complete"];
                break;
            case GnAcrStatusTypeQueryBegin:
                doShowStatus = ![[NSUserDefaults standardUserDefaults] boolForKey:ONLINE_KEY];
                message = [NSString stringWithFormat:@"Online Query Begin"];
                break;
            case GnAcrStatusTypeRecordingStarted:
                message = [NSString stringWithFormat:@"Recording Started"];
                break;
            case GnAcrStatusTypeDebug:
                doShowStatus = ![[NSUserDefaults standardUserDefaults] boolForKey:DEBUG_KEY];
                message = [NSString stringWithFormat:@"Debug %@)", status.message];
                break;
            case GnAcrStatusTypeTransition:
                doShowStatus = ![[NSUserDefaults standardUserDefaults] boolForKey:TRANSITION_KEY];
                message = [NSString stringWithFormat:@"Transition"];
                break;
            default:
                break;
        }
        
        if (message) {
            NSString *resultString = [NSString stringWithFormat:@"ACR Status: %@", message];
            NSLog(@"%@", resultString);
            //callback
        }
        
    }
}

-(BOOL)readBundleData:(void*)data
             capacity:(size_t)capacity
         numBytesRead:(size_t*)numBytesRead
{
    // Load the requested number of bytes into the given data buffer.
    // This callback will be called repeatedly until the ingestion is complete.
    
    NSData *fileData = [bundleFileHandle readDataOfLength:capacity];
    [fileData getBytes:data length:fileData.length];
    // report actual bytes read
    *numBytesRead = fileData.length;
    
    // the return value indicates whether an error occured and
    // that the ingestion should be aborted
    return NO;
}

@end