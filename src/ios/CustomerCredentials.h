/*
 * Copyright (c) 2012 Gracenote.
 *
 * This software may not be used in any way or distributed without
 * permission. All rights reserved.
 *
 * Some code herein may be covered by US and international patents.
 */


// You have been issued a Gracenote Client ID from Gracenote Professional Services.
// This client ID has the form XXXXXXXX-xxxxxxxxxxxxxxxxxxxxxxxxxxxxx

// Replace the CLIENT_ID below with the XXXXXXXX from your client ID
// Replace the CLIENT_ID_TAG below with the xxxxxxxxxxxxxxxxxxxxxxxxxxxxx from your client ID

// This Client ID/TAG is used for ACR/Video/EPG queries
#define CLIENT_ID_VIDEO               @"" // replace me
#define CLIENT_TAG_VIDEO           @"" // replace me


// MusicID queries require a separate client ID/TAG.
// Consult Gracenote Professional Severices for assistance.
#define ENABLE_MUSIC_ID 0
#if ENABLE_MUSIC_ID

#define CLIENT_ID_MUSIC             @"" // replace me
#define CLIENT_TAG_MUSIC         @"" // replace me

#endif



// You have also been issued a license file from GN Professional Services.
// replace the empty LICENSE_STRING below with the text from you license file.
// Line feeds should be replaced by the escape string "\r\n", e.g. @"this is line one\r\nThis is line 2"
// GN Professional Services can supply you with a properly formatted license string by request.

// If you wish to utilize both ACR/Video/EPG and MusicID in the same application, the
// license must contain entitlements for both clientIDs above. Consult Gracenote Professional
// Severices for assistance.

#define LICENSE_STRING            @""





