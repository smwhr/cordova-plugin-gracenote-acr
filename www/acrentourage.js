
var entourage = exports;

var exec    = require('cordova/exec');
var utils   = require('cordova/utils');

entourage.ACRInit = function(callback, callback_err){
    cordova.exec( callback
                , function(err) {
                    callback_err('Start failed. ' + err);
                 }
                , "TRYEntourageACR"
                , "TRYInit"
                , []);
}

entourage.ACRStart = function(callback, callback_err){
    cordova.exec( callback
                , function(err) {
                    callback_err('Start failed. ' + err);
                 }
                , "TRYEntourageACR"
                , "TRYStart"
                , []);
}

entourage.ACRStop = function(callback, callback_err){
    cordova.exec( callback
                , function(err) {
                    callback_err('Stop failed. ' + err);
                 }
                , "TRYEntourageACR"
                , "TRYStop"
                , []);
}

entourage.ACRLookup = function(src,callback, callback_err){
    cordova.exec( callback
                , function(err) {
                    callback_err('Lookup failed. ' + err);
                 }
                , "TRYEntourageACR"
                , "TRYLookup"
                , [src]);

}

